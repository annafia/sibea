<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>Login | <?= lang('title1'). lang('title2') ?></title>
    <link rel="apple-touch-icon" href="<?= base_url('dist') ?>/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('dist') ?>/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
    rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
    rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url('dist') ?>/app-assets/css/vendors.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url('dist') ?>/app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url('dist') ?>/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('dist') ?>/app-assets/css/pages/login-register.css">
    <!-- END Page Level CSS-->
</head>
<body class="vertical-layout vertical-compact-menu 1-column   menu-expanded blank-page blank-page"
data-open="click" data-menu="vertical-compact-menu" data-col="1-column">
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-md-4 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 m-0">
                                <div class="card-header border-0">
                                    <?php $this->load->view('partials/message') ?>
                                    <div class="card-title">
                                        <div class="col-md-12">
                                            <h1 align="center" style="font-weight: bold">
                                                <img width="60px" src="<?= base_url('dist') ?>/app-assets/images/logo/LOGO_BEA_CUKAI.png">
                                                <span style="color: blue"><?= lang('title1') ?></span> 
                                                <span style="color:red"> <?= lang('title2') ?></span>
                                            </h1>
                                            <br>
                                        </div>
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                                        <span align="center"><?= lang('welcome') ?></span>
                                    </h6>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form method="post" class="form-horizontal form-simple" action="<?= base_url('welcome/login') ?>">
                                            <fieldset class="form-group position-relative has-icon-left mb-0">
                                                <input type="text" class="form-control" name="username" placeholder="<?= lang('username').lang('anda') ?>" value="<?= $this->session->flashdata('username') ?>" required>
                                                <div class="form-control-position">
                                                    <i class="ft-user"></i>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="password" class="form-control" name="password" placeholder="<?= lang('input').lang('password') ?>" required>
                                                <div class="form-control-position">
                                                    <i class="la la-key"></i>
                                                </div>
                                            </fieldset>
                                            
                                            <button type="submit" class="btn btn-info btn-lg btn-block">
                                                <i class="ft-unlock"></i> <?= lang('masuk') ?>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-right font-small-1">
                                        <?= lang('developer') ?> <i class="ft-heart pink"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
  
    <!-- BEGIN VENDOR JS-->
    <script src="<?= base_url('dist') ?>/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="<?= base_url('dist') ?>/app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="<?= base_url('dist') ?>/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
</body>
</html>