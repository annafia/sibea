<li class=" nav-item" id="menu_dashboard">
	<a href="<?= base_url('dashboard') ?>">
		<i class="la la-home"></i>
		<span class="menu-title" data-i18n="nav.dash.main">Dashboard</span>
	</a>
</li>
<li class=" nav-item" id="menu_satuan">
	<a href="<?= base_url('satuan') ?>">
		<i class="la la-tags"></i>
		<span class="menu-title" data-i18n="nav.dash.main">Satuan</span>
	</a>
</li>