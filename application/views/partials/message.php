<?php if ($this->session->flashdata('warningMessage')) { ?>
<div class="alert alert-icon-right alert-arrow-right alert-warning alert-dismissible mb-2" role="alert">
	<span class="alert-icon"><i class="la la-warning"></i></span>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<?= $this->session->flashdata('warningMessage') ?>
</div>
<?php } ?>
<?php if ($this->session->flashdata('successMessage')) { ?>
<div class="alert alert-icon-right alert-arrow-right alert-success alert-dismissible mb-2" role="alert">
	<span class="alert-icon"><i class="la la-check"></i></span>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<?= $this->session->flashdata('successMessage') ?>
</div>
<?php } ?>
<?php if ($this->session->flashdata('errorMessage')) { ?>
<div class="alert alert-icon-right alert-arrow-right alert-danger alert-dismissible mb-2" role="alert">
	<span class="alert-icon"><i class="la la-close"></i></span>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<?= $this->session->flashdata('errorMessage') ?>
</div>
<?php } ?>
<?php if ($this->session->flashdata('infoMessage')) { ?>
<div class="alert alert-icon-right alert-arrow-right alert-info alert-dismissible mb-2" role="alert">
	<span class="alert-icon"><i class="la la-info-circle"></i></span>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<?= $this->session->flashdata('infoMessage') ?>
</div>
<?php } ?>