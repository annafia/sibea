<?php section('css'); ?>

<?php endsection(); ?>

<?php section('content') ?>
<div class="row">	
	<div class="col-xl-6 col-lg-6 col-12">
		<div class="card">
			<div class="card-content">
				<div class="card-body">
					<div class="media d-flex">
						<div class="align-self-center">
							<i class="icon-pencil info font-large-2 float-left"></i>
						</div>
						<div class="media-body text-right">
							<h3>278</h3>
							<span><?= lang('new_article') ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-6 col-lg-6 col-12">
		<div class="card">
			<div class="card-content">
				<div class="card-body">
					<div class="media d-flex">
						<div class="align-self-center">
							<i class="icon-speech warning font-large-2 float-left"></i>
						</div>
						<div class="media-body text-right">
							<h3>156</h3>
							<span><?= lang('new_comment') ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-6 col-lg-6 col-12">
		<div class="card">
			<div class="card-content">
				<div class="card-body">
					<div class="media d-flex">
						<div class="align-self-center">
							<i class="icon-user success font-large-2 float-left"></i>
						</div>
						<div class="media-body text-right">
							<h3>50</h3>
							<span><?= lang('new_user') ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-6 col-lg-6 col-12">
		<div class="card">
			<div class="card-content">
				<div class="card-body">
					<div class="media d-flex">
						<div class="align-self-center">
							<i class="icon-pointer danger font-large-2 float-left"></i>
						</div>
						<div class="media-body text-right">
							<h3>423</h3>
							<span><?= lang('total_visit') ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endsection()?>

<?php section('js'); ?>

<?php endsection(); ?>

<?php section('script'); ?>
<script>
	$('#menu_dashboard').addClass('active');
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>