<?php section('css'); ?>
<link rel="stylesheet" type="text/css" href="<?= base_url('dist') ?>/app-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url('dist') ?>/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
<?php endsection(); ?>

<?php section('content') ?>
<?= $this->load->view('partials/message') ?>
<div class="content-header row">
	<div class="content-header-left col-md-6 col-12 mb-2">
		<h3 class="content-header-title"><?= lang('daftar_satuan') ?></h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>">
						<?= lang('dashboard') ?></a>
					</li>
					<li class="breadcrumb-item active">
						<?= lang('daftar_satuan') ?>
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-12">
		<div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
			<a href="<?= base_url('satuan/tambah') ?>" class="btn btn-info round box-shadow-2 px-2">
				<i class="ft-plus icon-left"></i> Tambah Satuan
			</a>
		</div>
	</div>
</div>
<section id="constructor">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title"><?= lang('daftar_satuan') ?></h4>
					<a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="card-content collapse show">
					<div class="card-body card-dashboard">
						<table class="table table-striped table-bordered dataex-res-constructor">
							<thead>
								<tr>
									<th width="10%"><?= lang('no') ?></th>
									<th width="80%"><?= lang('nama_satuan') ?></th>
									<th width="10%"><?= lang('action') ?></th>
								</tr>
							</thead>
							<tbody>
								<?php $i=0; foreach ($satuan as $s) { ?>
									<tr>
										<td><?= ++$i ?></td>
										<td><?= $s['nama_satuan'] ?></td>
										<td>
											<div class="btn-group float-right" role="group" aria-label="Basic example">
					                            <a href="<?= base_url('satuan/edit/'.$s['id_satuan']) ?>" class="btn btn-sm btn-icon btn-outline-primary">
					                            	<i class="la la-edit"></i>
					                            </a>
					                            <button type="button" class="btn btn-sm btn-icon btn-outline-danger" onclick="del('<?= base_url('satuan/del/'.$s['id_satuan']) ?>')">
					                            	<i class="la la-trash"></i>
					                            </button>
					                        </div>
										</td>
									</tr>
								<?php } ?>
								<?php if($i==0){ ?>
									<tr>
										<td align="center" colspan="3"><?= lang('belum_ada_data') ?></td>
									</tr>
								<?php } ?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endsection()?>

<?php section('js'); ?>
<script src="<?= base_url('dist') ?>/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
<script src="<?= base_url('dist') ?>/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?= base_url('dist') ?>/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script>
	$('#menu_satuan').addClass('active');
	var tableConstructor = $('.dataex-res-constructor').DataTable();

    new $.fn.dataTable.Responsive(tableConstructor);
    function del(url){
		swal({
		    title: "Apakah Anda Yakin?",
		    text: "Data yang dihapus tidak dapat dikembalikan lagi !",
		    icon: "warning",
		    showCancelButton: true,
		    buttons: {
                cancel: {
                    text: "Tidak, Batalkan !",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Ya, Saya Yakin !",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
		    }
		}).then(isConfirm => {
		    if (isConfirm) {
		    	window.location.href = url;
		        swal("Terhapus !", "Data Berhasil Dihapus.", "success");
		    }
		});
	}
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>