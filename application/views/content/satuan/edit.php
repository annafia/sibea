<?php section('css'); ?>

<?php endsection(); ?>

<?php section('content') ?>
<?= $this->load->view('partials/message') ?>
<div class="content-header row">
	<div class="content-header-left col-md-6 col-12 mb-2">
		<h3 class="content-header-title"><?= lang('edit_satuan') ?></h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>">
						<?= lang('dashboard') ?></a>
					</li>
					<li class="breadcrumb-item"><a href="<?= base_url('satuan') ?>">
						<?= lang('daftar_satuan') ?></a>
					</li>
					<li class="breadcrumb-item active">
						<?= lang('edit_satuan') ?>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<section id="basic-form-layouts">
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-content collapse show">
					<div class="card-body">
						<form class="form" method="post" action="<?= base_url('satuan/edit_data')?>">
							<div class="form-body">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<h5><b><?= lang('nama_satuan') ?></b></h5>
											<input type="hidden" name="id_satuan" value="<?= $satuan['id_satuan'] ?>">
											<input name="nama_satuan" type="text" class="form-control" placeholder="<?= lang('nama_satuan') ?>" value="<?= $satuan['nama_satuan'] ?>" required>
										</div>
										<div class="float-right">
				                            <button type="submit" class="btn btn-md btn-icon btn-outline-info">
				                            	<i class="la la-save"></i>
				                            	<?= lang('save') ?>
				                            </button>
				                            <button type="reset" class="btn btn-md btn-icon btn-outline-danger">
				                            	<i class="la la-refresh"></i>
				                            	<?= lang('reset') ?>
				                            </button>
				                        </div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endsection()?>

<?php section('js'); ?>

<?php endsection(); ?>

<?php section('script'); ?>

<?php endsection(); ?>

<?php getview('layouts/template') ?>