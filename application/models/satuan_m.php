<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satuan_m extends CI_Model {
	function get(){
		$res = $this->db->get('satuan');

		return $res->result_array();
	}
	function tambah_data($data){
		$res = $this->db->insert('satuan',$data);
		return $res;
	}
	function find($id_satuan){
		$res = $this->db->where('id_satuan',$id_satuan)
						->get('satuan');
		return $res->row_array();
	}
	function edit_data($data){
		$res = $this->db->where('id_satuan',$data['id_satuan'])
						->update('satuan',$data);
		return $res;
	}
	function del($id_satuan){
		$res = $this->db->where('id_satuan',$id_satuan)
						->delete('satuan');
		return $res;
	}
}
?>