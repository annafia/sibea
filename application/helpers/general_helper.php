<?php 
function random_string($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function to_date($data){
	$year = substr($data, 0, 4);
	$moon = substr($data, 5,2);
	$day = substr($data, 8,2);
	$date = $day.'-'.$moon.'-'.$year;
	return $date;
}

function to_date_time($data){
	$year = substr($data, 0, 4);
	$moon = substr($data, 5,2);
	$day = substr($data, 8,2);
	$time = substr($data, 11);
	$date = $day.'-'.$moon.'-'.$year.' '.$time;
	return $date;
}

function jk($data){
	if($data == 1){
		echo 'Laki-laki';
	}else{
		echo 'Perempuan';
	}
}

function role($data){
	if($data == 0){
		echo 'Superuser';
	}if($data == 1){
		echo 'Admin';
	}if($data == 2){
		echo 'Reviewer';
	}if($data == 3){
		echo 'Check Plgiator';
	}if($data == 4){
		echo 'Author';
	}if($data == 5){
		echo 'Translator';
	}
}
function position($data){
	if($data==1){
		echo "Mahasiswa";
	} else if($data==2){
		echo "Dosen atau Tendik";
	}
}
function status_user($data){
	if($data == 0){ ?>
		<button type="button" class="btn btn-danger btn-glow btn-sm"><?= lang('nonactive') ?></button>
	<?php }else if($data == 1){ ?>
		<button type="button" class="btn btn-success btn-glow btn-sm"><?= lang('active') ?></button>
	<?php }
}
function status_published($data){
	if($data == 0){ ?>
		<button type="button" class="btn btn-outline-warning btn-glow btn-sm"><?= lang('saved') ?></button>
	<?php }else if($data == 1){ ?>
		<button type="button" class="btn btn-outline-success btn-glow btn-sm"><?= lang('published') ?></button>
	<?php }
}
function status_member($data){
	if($data == 1){
		echo 'Anggota';
	}else if($data == 2){
		echo 'Pembimbing 1';
	}else if($data == 3){
		echo 'Pembimbing 2';
	}else if($data == 4){
		echo 'Pembimbing 3';
	}
}
function status_member2($data){
	if($data == 1){
		return 'Anggota';
	}else if($data == 2){
		return 'Pembimbing 1';
	}else if($data == 3){
		return 'Pembimbing 2';
	}else if($data == 4){
		return 'Pembimbing 3';
	}
}
function generate_id($data){
	if($data>=10000){
		return $data;
	} else if($data>=1000){
		return "0".$data;
	} else if($data>=100){
		return "00".$data;
	} else if($data>=10){
		return "000".$data;
	} else{
		return "0000".$data;
	}
}

function privilege($data){
	if($data == 0){ ?>
		<button type="button" data-color="red" class="btn bg-red waves-effect"><?= lang('menunggu_persetujuan') ?></button>
	<?php }if($data == 1){ ?>
		<button type="button" data-color="green" class="btn bg-green waves-effect"><?= lang('telah_disetujui') ?></button>
	<?php } ?>
<?php 
}

function plagiarism($data){
	if($data == 0){ ?>
		<button type="button" data-color="red" class="btn bg-red waves-effect"><?= lang('belum') ?></button>
	<?php }if($data == 1){ ?>
		<button type="button" data-color="green" class="btn bg-green waves-effect"><?= lang('sudah') ?></button>
	<?php }if($data == 2){ ?>
		<button type="button" class="btn btn-warning waves-effect"><?= lang('rev') ?></button>
	<?php }if($data == 3){ ?>
		<button type="button" class="btn btn-orange waves-effect"><?= lang('proses') ?></button>
	<?php } ?>
<?php 
}

function decission_rev($data){
	if($data == ""){ ?>
		<button type="button" data-color="red" class="btn bg-red waves-effect"><?= lang('tidak_ada') ?></button>
	<?php }if($data == 1){ ?>
		<button type="button" class="btn btn-warning waves-effect"><?= lang('revisi') ?></button>
	<?php }if($data == 2){ ?>
		<button type="button" class="btn btn-success waves-effect"><?= lang('tdk') ?></button>
	<?php } ?>
<?php 
}

function status_paper($data){
	if($data == 0){ ?>
		<button type="button" data-color="red" class="btn bg-red waves-effect"><?= lang('proses_pemeriksaan') ?></button>
	<?php }if($data == 1){ ?>
		<button type="button" data-color="green" class="btn bg-green waves-effect"><?= lang('lolos_semua_seleksi') ?></button>
	<?php }if($data == 2){ ?>
		<button type="button" data-color="amber" class="btn bg-amber waves-effect"><?= lang('rev') ?></button>
	<?php }if($data == 3){ ?>
		<button type="button" data-color="amber" class="btn btn-success waves-effect"><?= lang('sudah_submit') ?></button>
	<?php }if($data == 4){ ?>
		<button type="button" class="btn btn-danger waves-effect"><?= lang('bukti_tidak_valid') ?></button>
	<?php }if($data == 5){ ?>
		<button type="button" class="btn btn-danger waves-effect"><?= lang('karya_ditolak') ?></button>
	<?php }if($data == 6){ ?>
		<button type="button" class="btn btn-danger waves-effect"><?= lang('menunggu_persetujuan') ?></button>
	<?php } ?>
<?php 
}

function status_karya_reviewer($data){
	if($data == 0){ ?>
		<button type="button" data-color="red" class="btn bg-red waves-effect"><?= lang('blm_validasi') ?></button>
	<?php }if($data == 1){ ?>
		<button type="button" data-color="green" class="btn bg-green waves-effect"><?= lang('tervalidasi') ?></button>
	<?php }if($data == 2){ ?>
		<button type="button" data-color="green" class="btn btn-warning waves-effect"><?= lang('revisi') ?></button>
	<?php }if($data == 3){ ?>
		<button type="button" data-color="green" class="btn btn-warning waves-effect"><?= lang('belum') ?></button>
	<?php }?>
<?php 
}

function reviewer($data){
	if($data == 0){ ?>
		<button type="button" data-color="red" class="btn bg-red waves-effect"><?= lang('belum') ?></button>
	<?php }if($data == 1){ ?>
		<button type="button" data-color="green" class="btn bg-green waves-effect"><?= lang('sudah') ?></button>
	<?php }if($data == 2){ ?>
		<button type="button" data-color="amber" class="btn bg-amber waves-effect"><?= lang('rev') ?></button>
	<?php }if($data == 3){ ?>
		<button type="button" class="btn bg-orange waves-effect"><?= lang('proses') ?></button>
	<?php }
} 
function translator($data){
	if($data == 0){ ?>
		<button type="button" data-color="red" class="btn bg-red waves-effect"><?= lang('belum') ?></button>
	<?php }if($data == 1){ ?>
		<button type="button" data-color="green" class="btn bg-green waves-effect"><?= lang('sudah') ?></button>
	<?php }if($data == 3){ ?>
		<button type="button" class="btn bg-orange waves-effect"><?= lang('proses') ?></button>
	<?php } ?>
<?php 
}
function measurement($data){
	if($data == 1){ ?>
		Sangat Bagus
	<?php }else if($data == 2){ ?>
		Bagus
	<?php }else if($data == 3){ ?>
		Cukup
	<?php }else if($data == 4){ ?>
		Jelek
	<?php }else{ ?>
		Tidak Ada
	<?php } ?>
<?php 
}

function getRealIP(){
  if (!empty($_SERVER['HTTP_CLIENT_IP'])){
    $ip=$_SERVER['HTTP_CLIENT_IP'];
  }
  elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
    $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
  }
  else{
    $ip=$_SERVER['REMOTE_ADDR'];
  }
  return $ip;
}

function getClientIP(){
	if (isset($_SERVER)){
	    if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
	        return $_SERVER["HTTP_X_FORWARDED_FOR"];

	    if (isset($_SERVER["HTTP_CLIENT_IP"]))
	        return $_SERVER["HTTP_CLIENT_IP"];

	    return $_SERVER["REMOTE_ADDR"];
	}

	if (getenv('HTTP_X_FORWARDED_FOR'))
	    return getenv('HTTP_X_FORWARDED_FOR');

	if (getenv('HTTP_CLIENT_IP'))
	    return getenv('HTTP_CLIENT_IP');

	return getenv('REMOTE_ADDR');
}
function submit_user($data){
	if($data == 1){
		return 'Mengaktifkan';
	}else if($data == 2){
		return 'Memblokir Akses';
	}else if($data == 3){
		return 'Menghapus Permanen';
	}
}
?>