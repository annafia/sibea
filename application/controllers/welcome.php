<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
        parent::__construct();      
        $this->load->language('general');
        $this->load->model('auth_m','am');
    }

	public function index(){
		if($this->session->userdata('email')){
			$this->redirect->guest('dashboard');
		}else{
			$this->load->view('auth/login');
		}
	}
	function login(){
		$data = $this->input->post();
		$cek = $this->am->check($data);
		if($cek){
			$this->session->set_userdata(array(
				'username'=>$cek['username'],
				'level'=>$cek['level']
				));
			redirect ('dashboard');
		}
		else {
			$this->session->set_flashdata($this->input->post());
			$this->session->set_flashdata('errorMessage',lang('invalid_pass'));
			redirect ();
		}
	}
	function logout(){
		$this->session->sess_destroy();
		redirect();
	}
}
