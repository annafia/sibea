<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satuan extends CI_Controller {
	public function __construct(){
       	parent::__construct();
		$this->load->language('general');
		$this->load->model('satuan_m','sm');

		if(!$this->session->userdata('username')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
    }

    function satuan_validation(){
    	$this->form_validation->set_rules('nama_satuan', 'Nama Satuan', 'required|trim');
    }

	public function index(){
		$this->load->view('content/satuan/index', array(
			'satuan' => $this->sm->get()
		));
	}
	function tambah(){
		$this->load->view('content/satuan/tambah');
	}
	function edit($id_satuan){
		$this->load->view('content/satuan/edit',array(
			"satuan" => $this->sm->find($id_satuan)
		));
	}
	function tambah_data(){
		$data = $this->input->post();
		$this->satuan_validation();
		if ($this->form_validation->run() != TRUE) {
			$this->session->set_flashdata('errorMessage',validation_errors());
			redirect('satuan/tambah');
		}

		$save = $this->sm->tambah_data($data);
		if($save){
			$this->session->set_flashdata('successMessage',lang('success'));
			redirect('satuan');
		} else{
			$this->session->set_flashdata('errorMessage',lang('failed'));
			redirect('satuan/tambah');
		}
	}
	function edit_data(){
		$data = $this->input->post();

		$this->satuan_validation();
		if ($this->form_validation->run() != TRUE) {
			$this->session->set_flashdata('errorMessage',validation_errors());
			redirect('satuan/edit/'.$data['id_satuan']);
		}

		$find = $this->sm->find($data['id_satuan']);
		if(!$find){
			$this->session->set_flashdata('errorMessage',lang('not_find'));
			redirect('satuan');
		}

		$save = $this->sm->edit_data($data);
		if($save){
			$this->session->set_flashdata('successMessage',lang('success'));
		} else{
			$this->session->set_flashdata('errorMessage',lang('failed'));
		}
		redirect('satuan/edit/'.$data['id_satuan']);
	}
	function del($id_satuan){
		$find = $this->sm->find($id_satuan);
		if(!$find){
			$this->session->set_flashdata('errorMessage',lang('not_find'));
			redirect('satuan');
		}

		$del = $this->sm->del($id_satuan);
		if($del){
			$this->session->set_flashdata('successMessage',lang('del_success'));
		} else{
			$this->session->set_flashdata('errorMessage',lang('failed'));
		}
		redirect('satuan');
	}
}
