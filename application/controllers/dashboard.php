<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct(){
       parent::__construct();
		$this->load->language('general');

		if(!$this->session->userdata('username')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
    }

	public function index(){
		$this->load->view('content/dashboard/index');
	}
}
