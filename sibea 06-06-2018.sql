/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 100113
 Source Host           : localhost:3306
 Source Schema         : sibea

 Target Server Type    : MySQL
 Target Server Version : 100113
 File Encoding         : 65001

 Date: 06/06/2018 12:02:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ba_cacah
-- ----------------------------
DROP TABLE IF EXISTS `ba_cacah`;
CREATE TABLE `ba_cacah`  (
  `id_bcc` int(11) NOT NULL AUTO_INCREMENT,
  `no_bcc` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `peruntukan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgl_bcc` date NOT NULL,
  `status_peruntukan` tinyint(4) NOT NULL,
  `create_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_bcc`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for barang
-- ----------------------------
DROP TABLE IF EXISTS `barang`;
CREATE TABLE `barang`  (
  `id_barang` int(11) NOT NULL AUTO_INCREMENT,
  `no_cn` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgl_cn` date NOT NULL,
  `jumlah` int(255) NOT NULL,
  `id_kemasan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_kategori` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `uraian` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `perkiraan_nilai` decimal(11, 0) NOT NULL,
  `pemilik` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `file_btd` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `create_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_barang`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions`  (
  `session_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ip_address` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_agent` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `last_activity` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_data` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('55acc886b277efd531bed4996adab99d', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '1528256274', 'a:5:{s:9:\"user_data\";s:0:\"\";s:10:\"currentURL\";s:9:\"dashboard\";s:8:\"username\";s:3:\"POS\";s:5:\"level\";s:1:\"2\";s:7:\"backURL\";s:9:\"dashboard\";}');
INSERT INTO `ci_sessions` VALUES ('5e9509830c6933c5a4c9e180038cf316', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', '1528261323', 'a:4:{s:10:\"currentURL\";s:0:\"\";s:7:\"backURL\";s:6:\"satuan\";s:8:\"username\";s:3:\"pos\";s:5:\"level\";s:1:\"1\";}');

-- ----------------------------
-- Table structure for kategori
-- ----------------------------
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori`  (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_kategori`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kategori
-- ----------------------------
INSERT INTO `kategori` VALUES (1, 'Makanan');
INSERT INTO `kategori` VALUES (2, 'Minuman');
INSERT INTO `kategori` VALUES (3, 'BKC (Barang Kena Cukai)');
INSERT INTO `kategori` VALUES (4, 'Obat dan Suplemen');
INSERT INTO `kategori` VALUES (5, 'Kosmetik');
INSERT INTO `kategori` VALUES (6, 'Pakaian');
INSERT INTO `kategori` VALUES (7, 'Lain-Lain');

-- ----------------------------
-- Table structure for kpknl
-- ----------------------------
DROP TABLE IF EXISTS `kpknl`;
CREATE TABLE `kpknl`  (
  `id_kpknl` int(11) NOT NULL AUTO_INCREMENT,
  `id_bcc` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_kpknl` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgl_kpknl` date NOT NULL,
  `file_kpknl` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `create_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_kpknl`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for pegawai
-- ----------------------------
DROP TABLE IF EXISTS `pegawai`;
CREATE TABLE `pegawai`  (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_peg` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jabatan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `pangkat` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `gol` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_pegawai`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 95 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pegawai
-- ----------------------------
INSERT INTO `pegawai` VALUES (1, 'Rudy Hery Kurniawan', '196809261988121001', 'Kepala Kantor', 'Pembina Tk. I', 'IV/b');
INSERT INTO `pegawai` VALUES (2, 'Surjaningsih', '196906091996032001', 'Kepala Seksi Penyuluhan dan Layanan Informasi', 'Pembina', 'IV/a');
INSERT INTO `pegawai` VALUES (3, 'Tri Hartana', '197101011990121001', 'Kepala Seksi Penyidikan dan Barang Hasil Penindakan', 'Pembina', 'IV/a');
INSERT INTO `pegawai` VALUES (4, 'Indasah\r\n', '196805101988122001', 'Kepala Seksi Perbendaharaan\r\n', 'Penata Tk. I', 'III/d');
INSERT INTO `pegawai` VALUES (5, 'Subekti', '197008251990121001', 'Kepala Seksi Kepatuhan Internal\r\n', 'Penata Tk. I', 'III/d');
INSERT INTO `pegawai` VALUES (6, 'Raden Pandam Prihandarko Hambodo\r\n', '197101151992011001', 'Kepala Seksi Pelayanan Kepabeanan dan Cukai I\r\n', 'Penata Tk. I', 'III/d');
INSERT INTO `pegawai` VALUES (7, 'I Nyoman Tantri Phala\r\n', '197404021994021001', 'Kepala Subbagian Umum\r\n', 'Penata Tk. I', 'III/d');
INSERT INTO `pegawai` VALUES (8, 'Arief Hartono\r\n', '197505031995031002', 'Kepala Seksi Intelijen dan Penindakan\r\n', 'Penata Tk. I ', 'III/d');
INSERT INTO `pegawai` VALUES (9, 'Beni Sutono\r\n', '197610201999031002', 'Kepala Seksi Pelayanan Kepabeanan dan Cukai II\r\n', 'Penata Tk. I ', 'III/d');
INSERT INTO `pegawai` VALUES (10, 'Hasrifah\r\n', '197006141990122002', 'Kepala Urusan Rumah Tangga dan Dukungan Teknis\r\n', 'Penata Tk. I ', 'III/d');
INSERT INTO `pegawai` VALUES (11, 'Agnita Adityawardani\r\n', '197508011995022001', 'Kepala Urusan Keuangan\r\n', 'Penata Tk. I', 'III/d');
INSERT INTO `pegawai` VALUES (12, 'Lilik Prahastutie\r\n', '196308301983032001', 'Kepala Subseksi Hanggar Pabean dan Cukai V\r\n', 'Penata Tk. I', 'III/d');
INSERT INTO `pegawai` VALUES (13, 'Puput Pujo Lestoro\r\n', '197409071995031002', 'Kepala Subseksi Layanan Informasi\r\n', 'Penata Tk. I', 'III/d');
INSERT INTO `pegawai` VALUES (14, 'Eddy Setyo Sugiharto\r\n', '196205011983031001', 'Kepala Subseksi Hanggar Pabean dan Cukai VI\r\n', 'Penata Tk. I', 'III/d');
INSERT INTO `pegawai` VALUES (15, 'Saiful Anan\r\n', '197205011992121001', 'Kepala Subseksi Administrasi Penagihan dan Pengembalian\r\n', 'Penata', 'III/c');
INSERT INTO `pegawai` VALUES (16, 'Tri Murni Handayani\r\n', '197208221992122001', 'Kepala Urusan Tata Usaha dan Kepegawaian\r\n', 'Penata', 'III/c');
INSERT INTO `pegawai` VALUES (17, 'Esti Dyah Palupi\r\n', '197405051995032001', 'Kepala Subseksi Hanggar Pabean dan Cukai I\r\n', 'Penata', 'III/c');
INSERT INTO `pegawai` VALUES (18, 'Taupan Andreanus Rori Saputra\r\n', '197807222000011002', 'Kepala Subseksi Penindakan I\r\n', 'Penata', 'III/c');
INSERT INTO `pegawai` VALUES (19, 'Edhi Riyanto\r\n', '197507161999031002', 'Kepala Subseksi Administrasi dan Distribusi Pita Cukai\r\n', 'Penata', 'III/c');
INSERT INTO `pegawai` VALUES (20, 'I Made Suada\r\n', '196312311983031019', 'Kepala Subseksi Hanggar Pabean dan Cukai IV\r\n', 'Penata Muda Tk. I', 'III/b');
INSERT INTO `pegawai` VALUES (21, 'Andyk Budi W. S. Putra\r\n', '197109281992011001', 'Kepala Subseksi Kepatuhan Pelaksanaan Tugas Pelayanan dan Administrasi\r\n', 'Penata Muda Tk. I', 'III/b');
INSERT INTO `pegawai` VALUES (22, 'Krisno Budi Bagus Sasmito\r\n', '197701031997031001', 'Kepala Subseksi Sarana Operasi\r\n', 'Penata Muda Tk. I', 'III/b');
INSERT INTO `pegawai` VALUES (23, 'Rio Al Faris Rahman Hakim\r\n', '198104072003121002', 'Kepala Subseksi Kepatuhan Pelaksanaan Tugas Pengawasan\r\n', 'Penata Muda Tk. I', 'III/b');
INSERT INTO `pegawai` VALUES (24, 'Jims Oktovianus\r\n', '198210212003121001', 'Kepala Subseksi Penyidikan\r\n', 'Penata Muda Tk. I', 'III/b');
INSERT INTO `pegawai` VALUES (25, 'Ajar Septian Aditama', '198509062007011001', 'Kepala Subseksi Penindakan II\r\n', 'Penata Muda Tk. I', 'III/b');
INSERT INTO `pegawai` VALUES (26, 'Chandra Dwi Nanta Biantoro\r\n', '198606042010121006', 'Kepala Subseksi Penyuluhan\r\n', 'Penata Muda Tk. I', 'III/b');
INSERT INTO `pegawai` VALUES (27, 'Widyatmoko\r\n', '197904161999031006', 'Kepala Subseksi Hanggar Pabean dan Cukai III\r\n', 'Penata Muda ', 'III/a');
INSERT INTO `pegawai` VALUES (28, 'Tomy Liling\r\n', '197906241998031001', 'Kepala Subseksi Administrasi Penerimaan dan Jaminan\r\n', 'Penata Muda ', 'III/a');
INSERT INTO `pegawai` VALUES (29, 'Bendito Menezes', '197901111999031001', 'Kepala Subseksi Administrasi Barang Hasil Penindakan\r\n', 'Penata Muda ', 'III/a');
INSERT INTO `pegawai` VALUES (30, 'Marthinus Atamau\r\n', '197605291999031001', 'Kepala Subseksi Intelijen\r\n', 'Penata Muda ', 'III/a');
INSERT INTO `pegawai` VALUES (31, 'Chanafi\r\n', '196004281983091001', 'Analis Penindakan Junior\r\n', 'Penata', 'III/c');
INSERT INTO `pegawai` VALUES (32, 'Isti Sayekti\r\n', '196004221983032002', 'Pemeriksa Barang Tk. I\r\n', 'Penata Muda Tk. I', 'III/b');
INSERT INTO `pegawai` VALUES (33, 'Retnaningsih\r\n', '196304131983032001', 'Pemeriksa Barang Tk. I\r\n', 'Penata Muda Tk. I', 'III/b');
INSERT INTO `pegawai` VALUES (34, 'Hitamawan Robertho\r\n', '197610221997031002', 'Pengolah Data Penindakan Senior\r\n', 'Penata Muda ', 'III/a');
INSERT INTO `pegawai` VALUES (35, 'Didit Hardi Ma\'rufi\r\n', '198406152003121004', 'Pemeriksa Barang Tk. II \r\n', 'Penata Muda ', 'III/a');
INSERT INTO `pegawai` VALUES (36, 'Kristian Agung Pramono\r\n', '198908272010121003', 'Pemroses Bahan Penyuluhan Senior\r\n', 'Penata Muda ', 'III/a');
INSERT INTO `pegawai` VALUES (37, 'Nory Fitriana Eka Putri\r\n', '198812082014022001', 'Pengelola Data Penagihan dan Pengembalian Junior\r\n', 'Penata Muda ', 'III/a');
INSERT INTO `pegawai` VALUES (38, 'Witra Abdilah Sapta Tinafi\r\n', '198611292008121004', 'Pemeriksa Barang Tk.III\r\n', 'Penata Muda', 'III/a');
INSERT INTO `pegawai` VALUES (39, 'Andri Yulianto\r\n', '198707182008121004', 'Pemeriksa Barang Tk.III\r\n', 'Penata Muda ', 'III/a');
INSERT INTO `pegawai` VALUES (40, 'Wayan Eri Wasgita\r\n', '198804052008121001', 'Pemeriksa Barang Tk.III\r\n', 'Penata Muda ', 'III/a');
INSERT INTO `pegawai` VALUES (41, 'Abdul Malik Zulkarnain\r\n', '198711032007011003', 'Pengelola Data Penagihan dan Pengembalian Junior\r\n', 'Penata Muda ', 'III/a');
INSERT INTO `pegawai` VALUES (42, 'Diky Adi Saputra\r\n', '199107112018011001', 'Analis Bea dan Cukai\r\n', 'Penata Muda', 'III/a');
INSERT INTO `pegawai` VALUES (43, 'Rizha Febriyanti\r\n', '199202222018012002', 'Analis Bea dan Cukai\r\n', 'Penata Muda', 'III/a');
INSERT INTO `pegawai` VALUES (44, 'Akhmad Shonhaji\r\n', '198209232003121002', 'Penyaji Data Barang Hasil Penindakan Senior \r\n', 'Pengatur Tk. I', 'II/d');
INSERT INTO `pegawai` VALUES (45, 'Robianto Kasih\r\n', '198309082003121002', 'Pengolah Data Tata Usaha dan Kepegawaian Junior\r\n', 'Pengatur Tk. I', 'II/d');
INSERT INTO `pegawai` VALUES (46, 'Wendy Dwi Nata\r\n', '198703222006021003', 'Penyaji Data Rumah Tangga dan Dukungan Teknis Senior\r\n', 'Pengatur Tk. I', 'II/d');
INSERT INTO `pegawai` VALUES (47, 'Septian Viga Rakhmantika\r\n', '198609212007101001', 'Penata Usaha Senior\r\n', 'Pengatur', 'II/c');
INSERT INTO `pegawai` VALUES (48, 'Bagus Murdi Yansyah\r\n', '198705072007101001', 'Pelaksana Tugas Belajar Tk.V\r\n', 'Pengatur', 'II/c');
INSERT INTO `pegawai` VALUES (49, 'Sapuan Puji Rianto Setiawan\r\n', '198705272007101002', 'Penyaji Data Intelijen Junior\r\n', 'Pengatur', 'II/c');
INSERT INTO `pegawai` VALUES (50, 'Yedija Derven Kamau\r\n', '198711142007101001', 'Penyaji Data Penerimaan dan Jaminan Senior\r\n', 'Pengatur', 'II/c');
INSERT INTO `pegawai` VALUES (51, 'Bayu Widhianto\r\n', '198712102007101001', 'Penata Usaha Senior  \r\n', 'Pengatur', 'II/c');
INSERT INTO `pegawai` VALUES (52, 'Agung Pamungkas\r\n', '198812212007101001', 'Penata Usaha Senior  \r\n', 'Pengatur', 'II/c');
INSERT INTO `pegawai` VALUES (53, 'Tri Ahmad Rudiasta\r\n', '199503082016121001', 'Pengolah Data', 'Pengatur', 'II/c');
INSERT INTO `pegawai` VALUES (54, 'Erwinda Yolidya Puspitaningrum\r\n', '199507222016122001', 'Pengolah Data\r\n', 'Pengatur', 'II/c');
INSERT INTO `pegawai` VALUES (55, 'Destian Cahya Putra\r\n', '199512262018011003', 'Pengolah Data\r\n', 'Pengatur', 'II/c');
INSERT INTO `pegawai` VALUES (56, 'Nur Cahyono\r\n', '199501312018011003', 'Pengolah Data\r\n', 'Pengatur', 'II/c');
INSERT INTO `pegawai` VALUES (57, 'Dhisya Citta Septiana\r\n', '199509012018012003', 'Pengolah Data\r\n', 'Pengatur', 'II/c');
INSERT INTO `pegawai` VALUES (58, 'Rizal Shalahuddin Mahardhika\r\n', '198906252010011002', 'Bendahara (masa kerja 1 s.d kurang dari 2 tahun)\r\n', 'Pengatur Muda Tk. I', 'II/b');
INSERT INTO `pegawai` VALUES (59, 'Dicky Roni Wirawan\r\n', '199105102010121004', 'Penata Usaha Senior\r\n', 'Pengatur Muda Tk. I', 'II/b');
INSERT INTO `pegawai` VALUES (60, 'Oktavian Aidar Yoga Pradita\r\n', '198910152010121006', 'Penata Usaha Senior\r\n', 'Pengatur Muda Tk. I', 'II/b');
INSERT INTO `pegawai` VALUES (61, 'Arif Fitrianto\r\n', '198905072010011001', 'Penyaji Data Barang Hasil Penindakan Junior\r\n', 'Pengatur Muda Tk. I', 'II/b');
INSERT INTO `pegawai` VALUES (62, 'Deni Setiawan\r\n', '199112172012101002', 'Penata Usaha Senior\r\n', 'Pengatur Muda Tk. I', 'II/b');
INSERT INTO `pegawai` VALUES (63, 'Dimas Ade Nugroho\r\n', '199112182012101002', 'Penata Usaha Senior\r\n', 'Pengatur Muda Tk. I', 'II/b');
INSERT INTO `pegawai` VALUES (64, 'R. Kautsar Firdausi\r\n', '199112112012101001', 'Penata Usaha Senior\r\n', 'Pengatur Muda Tk. I', 'II/b');
INSERT INTO `pegawai` VALUES (65, 'Kiky Zakariya\r\n', '199206152012101003', 'Penata Usaha Senior\r\n', 'Pengatur Muda Tk. I', 'II/b');
INSERT INTO `pegawai` VALUES (66, 'Farda Ashidiki', '199208092012101001', 'Penata Usaha Senior\r\n', 'Pengatur Muda Tk. I', 'II/b');
INSERT INTO `pegawai` VALUES (67, 'Riza Mahardhika Sidha\r\n', '199212272013101002', 'Penata Usaha Junior\r\n', 'Pengatur Muda Tk. I', 'II/b');
INSERT INTO `pegawai` VALUES (68, 'Trianugrah Yuphi Putranto\r\n', '199301232013101001', 'Penata Usaha Junior\r\n', 'Pengatur Muda Tk. I', 'II/b');
INSERT INTO `pegawai` VALUES (69, 'Abd. Hakim Haris\r\n', '199310052015021001', 'Penata Usaha Junior\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (70, 'Agung Muhamad Reza\r\n', '199505102015021005', 'Penata Usaha Junior\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (71, 'Alfernando Fitra Wijaya\r\n', '199503042015021001', 'Penata Usaha Junior\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (72, 'Andra Kurniawan\r\n', '199504242015021001', 'Penata Usaha Junior', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (73, 'Bagaswara Aria Rikhaldi\r\n', '199409282015021001', 'Penata Usaha Junior\r\n\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (74, 'Bimo Aryo Kencono\r\n', '199408272015021001', 'Penata Usaha Junior', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (75, 'Carolus Rexi Domuceras Silaban\r\n', '199511042015021002', 'Penata Usaha Junior\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (76, 'Ekky Dio Kurnia Gani\r\n', '199506252015021002', 'Penata Usaha Junior', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (77, 'Haryo Adiyono Dharmawan\r\n', '199408302015021001', 'Penata Usaha Junior', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (78, 'Imam Sutoyo\r\n', '199411242015021003', 'Penata Usaha Junior\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (79, 'Polin Troy Napitupulu\r\n', '199505032015021006', 'Penata Usaha Junior\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (80, 'Primadifta Pancadasa Merdeka P.\r\n', '199508132015021006', 'Penata Usaha Junior\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (81, 'Vico Fajar Nur Arfian\r\n', '199506122015021002', 'Penata Usaha Junior\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (82, 'Wahyu Jati Pamungkas \r\n', '199309112015021002', 'Penata Usaha Junior\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (83, 'Gilbert Tasidjawa\r\n', '', 'Penata Usaha Junior\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (84, 'Avan Gardy\r\n', '199505072015121001', 'Penata Usaha Pemula', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (85, 'Galoeh Widia Hapsari\r\n', '199511292015122002', 'Penata Usaha Pemula\r\n', 'Pengatur Muda ', 'II/a');
INSERT INTO `pegawai` VALUES (86, 'Tsabita Gladys Suwarno\r\n', '199602192015122002', 'Penata Usaha Pemula\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (87, 'Kingkin Kintan Kumala\r\n', '199609102015122002', 'Penata Usaha Pemula\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (88, 'Lisna Puspitasari \r\n', '199602222015122001', 'Penata Usaha Pemula\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (89, 'Muhammad Rizki Romdhoni\r\n', '199801052016121001', 'Penata Usaha Pemula\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (90, 'Al Hadiid Faudji\r\n', '199710022018011003', 'Pengadministrasi Umum\r\n\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (91, 'Firman Tri Raharjo \r\n', '199804212018011003', 'Pengadministrasi Umum\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (92, 'Hamida Noer Hasna Aisya \r\n', '199804202018012001', 'Pengadministrasi Umum\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (93, 'Muhammad Tasbihul Insani\r\n', '199709172018011001', 'Pengadministrasi Umum\r\n', 'Pengatur Muda', 'II/a');
INSERT INTO `pegawai` VALUES (94, 'Nafi\'ah Indah Mutiara\r\n', '199803192018012 001', 'Pengadministrasi Umum\r\n', 'Pengatur Muda', 'II/a');

-- ----------------------------
-- Table structure for satuan
-- ----------------------------
DROP TABLE IF EXISTS `satuan`;
CREATE TABLE `satuan`  (
  `id_satuan` int(10) NOT NULL AUTO_INCREMENT,
  `nama_satuan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_satuan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of satuan
-- ----------------------------
INSERT INTO `satuan` VALUES (6, 'tonsss');
INSERT INTO `satuan` VALUES (8, 'pcs');
INSERT INTO `satuan` VALUES (9, 'asdasda');
INSERT INTO `satuan` VALUES (10, 'sdafas');
INSERT INTO `satuan` VALUES (11, 'asdasad');
INSERT INTO `satuan` VALUES (12, 'asasdad');
INSERT INTO `satuan` VALUES (13, 'asdada');
INSERT INTO `satuan` VALUES (14, 'ML');

-- ----------------------------
-- Table structure for skep
-- ----------------------------
DROP TABLE IF EXISTS `skep`;
CREATE TABLE `skep`  (
  `id_skep` int(11) NOT NULL AUTO_INCREMENT,
  `id_bcc` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_skep` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jenis_skep` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_skep`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tim_pelaksana
-- ----------------------------
DROP TABLE IF EXISTS `tim_pelaksana`;
CREATE TABLE `tim_pelaksana`  (
  `id_tp` int(11) NOT NULL AUTO_INCREMENT,
  `id_skep` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ketua` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `wakil` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `bendahara` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sekretaris1` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sekretaris2` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ketua_perkap` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `anggota_perkap` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ketua_kemanan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `anggota_kemanan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ketua_pdd` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `anggota_pdd` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_tp`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id_user` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_lengkap` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `repassword` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `level` tinyint(11) NOT NULL,
  PRIMARY KEY (`id_user`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'pos', 'Admin POS', '5e0bdcbddccca4d66d74ba8c1cee1a68', 'pos', 1);
INSERT INTO `user` VALUES (2, 'bea_cukai', 'Admin Bea Cukai', 'e2c4afed32f638fff2e9d63596a050ab', 'bea_cukai', 2);

SET FOREIGN_KEY_CHECKS = 1;
